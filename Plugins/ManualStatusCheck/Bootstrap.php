<?php
/*
START LICENSE AND COPYRIGHT

 This file is part of translate5
 
 Copyright (c) 2013 - 2015 Marc Mittag; MittagQI - Quality Informatics;  All rights reserved.

 Contact:  http://www.MittagQI.com/  /  service (ATT) MittagQI.com

 This file may be used under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE version 3
 as published by the Free Software Foundation and appearing in the file agpl3-license.txt 
 included in the packaging of this file.  Please review the following information 
 to ensure the GNU AFFERO GENERAL PUBLIC LICENSE version 3.0 requirements will be met:
 http://www.gnu.org/licenses/agpl.html

 There is a plugin exception available for use with this release of translate5 for
 open source applications that are distributed under a license other than AGPL:
 Please see Open Source License Exception for Development of Plugins for translate5
 http://www.translate5.net/plugin-exception.txt or as plugin-exception.txt in the root
 folder of translate5.
  
 @copyright  Marc Mittag, MittagQI - Quality Informatics
 @author     MittagQI - Quality Informatics
 @license    GNU AFFERO GENERAL PUBLIC LICENSE version 3 with plugin-execptions
			 http://www.gnu.org/licenses/agpl.html http://www.translate5.net/plugin-exception.txt

END LICENSE AND COPYRIGHT
*/

/**#@+
 * @author Marc Mittag
 * @package editor
 * @version 1.0
 *
 */
/**
 * Plugin Bootstrap for ManualStatusCheck Plugin
 * This Plugin checks if all segments has a manual status set. If not, the task can not be finished.
 */
class editor_Plugins_ManualStatusCheck_Bootstrap extends ZfExtended_Plugin_Abstract {
    
    public function init() {
        $this->eventManager->attach('editor_Workflow_Abstract', 'beforeFinish', array($this, 'handleBeforeFinish'));
    }
    
    /**
     * handler for event: editor_Workflow_Abstract#handleBeforeFinish
     * @param $event Zend_EventManager_Event
     */
    public function handleBeforeFinish(Zend_EventManager_Event $event) {
        $tua = $event->getParam('oldTua');
        /* @var $tua editor_Models_TaskUserAssoc */
        
        $segment = ZfExtended_Factory::get('editor_Models_Db_Segments');
        /* @var $segment editor_Models_Db_Segments */
        
        $s = $segment->select()
            ->from($segment, array('unsetStatusCount' => 'COUNT(id)'))
            ->where('stateId = 0')
            ->where('editable = 1')
            ->where('taskGuid = ?', $tua->getTaskGuid());
        
        $row = $segment->fetchRow($s);
        if($row->unsetStatusCount > 0) {
            $msg = 'Der Task kann nicht abgeschlossen werden, da nicht alle Segmente einen Status gesetzt haben. Bitte verwenden Sie die Filterfunktion um die betroffenen Segmente zu finden.';
            $e = new ZfExtended_NotAcceptableException($msg);
            $e->setMessage($msg, true);
            $e->setLogging(false);
            throw $e;
        }
    }
}