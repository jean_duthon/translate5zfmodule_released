<?php
/*
START LICENSE AND COPYRIGHT

 This file is part of translate5
 
 Copyright (c) 2013 - 2015 Marc Mittag; MittagQI - Quality Informatics;  All rights reserved.

 Contact:  http://www.MittagQI.com/  /  service (ATT) MittagQI.com

 This file may be used under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE version 3
 as published by the Free Software Foundation and appearing in the file agpl3-license.txt 
 included in the packaging of this file.  Please review the following information 
 to ensure the GNU AFFERO GENERAL PUBLIC LICENSE version 3.0 requirements will be met:
 http://www.gnu.org/licenses/agpl.html

 There is a plugin exception available for use with this release of translate5 for
 open source applications that are distributed under a license other than AGPL:
 Please see Open Source License Exception for Development of Plugins for translate5
 http://www.translate5.net/plugin-exception.txt or as plugin-exception.txt in the root
 folder of translate5.
  
 @copyright  Marc Mittag, MittagQI - Quality Informatics
 @author     MittagQI - Quality Informatics
 @license    GNU AFFERO GENERAL PUBLIC LICENSE version 3 with plugin-execptions
			 http://www.gnu.org/licenses/agpl.html http://www.translate5.net/plugin-exception.txt

END LICENSE AND COPYRIGHT
*/

/**#@+
 * @author Marc Mittag
 * @package editor
 * @version 1.0
 *
 */
/**
 * Segment keylog Entity Objekt
 */
class editor_Models_Segmentskeylog extends ZfExtended_Models_Entity_Abstract {
  protected $dbInstanceClass = 'editor_Models_Db_Segmentskeylog';

    /**
     * loads the Entity by Unique Key
     * @param segmentId , taskGuid, userGuid
     * @return Zend_Db_Table_Row
     */
    public function loadByFK($segmentId, $taskGuid, $userGuid) {
        try {
            $s = $this->db->select()->where('taskGuid = ?', $taskGuid)->where('userGuid = ?', $userGuid)->where('segmentId = ?', $segmentId);
            $row = $this->db->fetchRow($s);
        } catch (Exception $e) {
            $this->notFound('NotFound after other Error', $e);
        }
        //load implies loading one Row, so use only the first row
        if($row){
            return $this->row = $row;
        }
    }

    public function loadByTaskGuid($taskGuid){
        $s = $this->db->select()->where('taskGuid = ?', $taskGuid);
        $this->rows = $this->db->fetchAll($s);
    }

    public function getJSON(){
        $retjson = array();
        foreach($this->rows as $row){
            $rowArr = $row->toArray();
            $karr = $rowArr["segmentId"];
            $rowArr["log"] = json_decode($rowArr["log"]);
            error_log(print_r($rowArr, true));
            if(!array_key_exists($karr, $retjson)){
                $retjson[$karr] = array();
            }
            array_push($retjson[$karr], $rowArr);
        }
        return json_encode($retjson);
    }

}