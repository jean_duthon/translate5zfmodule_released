<?php
/*
START LICENSE AND COPYRIGHT

 This file is part of translate5
 
 Copyright (c) 2013 - 2015 Marc Mittag; MittagQI - Quality Informatics;  All rights reserved.

 Contact:  http://www.MittagQI.com/  /  service (ATT) MittagQI.com

 This file may be used under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE version 3
 as published by the Free Software Foundation and appearing in the file agpl3-license.txt 
 included in the packaging of this file.  Please review the following information 
 to ensure the GNU AFFERO GENERAL PUBLIC LICENSE version 3.0 requirements will be met:
 http://www.gnu.org/licenses/agpl.html

 There is a plugin exception available for use with this release of translate5 for
 open source applications that are distributed under a license other than AGPL:
 Please see Open Source License Exception for Development of Plugins for translate5
 http://www.translate5.net/plugin-exception.txt or as plugin-exception.txt in the root
 folder of translate5.
  
 @copyright  Marc Mittag, MittagQI - Quality Informatics
 @author     MittagQI - Quality Informatics
 @license    GNU AFFERO GENERAL PUBLIC LICENSE version 3 with plugin-execptions
			 http://www.gnu.org/licenses/agpl.html http://www.translate5.net/plugin-exception.txt

END LICENSE AND COPYRIGHT
*/

/**
 *
 */
class editor_WorkflowuserprefController extends ZfExtended_RestController {
    protected $entityClass = 'editor_Models_Workflow_Userpref';
    
    /**
     * @var editor_Models_Workflow_Userpref
     */
    protected $entity;
    
    /**
     * overridden to prepare data
     * (non-PHPdoc)
     * @see ZfExtended_RestController::decodePutData()
     */
    protected function decodePutData() {
        parent::decodePutData();

        //THIS PART SHOULD NOT BE NECESSARY
        if(isset($this->data->notEditContent) && $this->data->notEditContent == ""){
            unset($this->data->notEditContent);
        }
        if(isset($this->data->anonymousCols) && $this->data->anonymousCols == ""){
            unset($this->data->anonymousCols);
        }
        if(isset($this->data->frozen) && $this->data->frozen == ""){
            error_log("UNSETTING DATA FROZEN");
            $this->data->frozen = 0;
        }
        //END

        if($this->_request->isPost()) {
            unset($this->data->id); //don't set the ID from client side
            //a new default entry cannot be created, also a workflow step must always be set!
            if(empty($this->data->workflowStep) || empty($this->data->workflowStep) && empty($this->data->userGuid)) {
                throw new ZfExtended_Models_Entity_NotAcceptableException();
            }
        }
        if($this->_request->isPut()) {
            //we cant update an existing userpref entry to workflow step = null,
            //since only the default entry can have an empty worflow step
            if(property_exists($this->data, 'workflowStep') && empty($this->data->workflowStep)) {
                throw new ZfExtended_Models_Entity_NotAcceptableException();
            }
            if($this->entity->isDefault()) {
                unset($this->data->workflowStep); //don't update the workflowStep of the default entry
                unset($this->data->userGuid); //don't update the userGuid of the default entry
            }
        }
        if(isset($this->data->frozen) && $this->data->frozen){
            $sessionUser = new Zend_Session_Namespace('user');
            $userGuid = $sessionUser->data->userGuid;
            $this->data->frozenBy = $userGuid;
            // Overwrite the saved state with the current one from the user
            //TODO FIXME This is kind of duplicated from StateProviderController
            $stateProviderEntity = ZfExtended_Factory::get('editor_Models_StateProvider');
            $UIDatas = $stateProviderEntity->loadByTaskAndUserUIData($this->data->taskGuid,$userGuid);
            foreach($UIDatas as $itemID => $value){
                $stateProviderEntity->loadIfExists($itemID, $this->data->taskGuid, $this->data->userGuid);
                $stateProviderEntity->setItemID($itemID);
                $stateProviderEntity->setStateValue($value);
                $stateProviderEntity->setTaskGuid($this->data->taskGuid);
                $stateProviderEntity->setUserGuid($this->data->userGuid);
                $stateProviderEntity->save();
            }
        }
    }
    
    /**
     * deletes the UserPref entry, ensures that the default entry cannot be deleted by API!
     * (non-PHPdoc)
     * @see ZfExtended_RestController::deleteAction()
     */
    public function deleteAction() {
        $this->entity->load($this->_getParam('id'));
        if($this->entity->isDefault()) {
            throw new ZfExtended_Models_Entity_NoAccessException();
        }
        $this->processClientReferenceVersion();
        $this->entity->delete();
    }
}