<?php
/*
START LICENSE AND COPYRIGHT

 This file is part of translate5
 
 Copyright (c) 2013 - 2015 Marc Mittag; MittagQI - Quality Informatics;  All rights reserved.

 Contact:  http://www.MittagQI.com/  /  service (ATT) MittagQI.com

 This file may be used under the terms of the GNU AFFERO GENERAL PUBLIC LICENSE version 3
 as published by the Free Software Foundation and appearing in the file agpl3-license.txt 
 included in the packaging of this file.  Please review the following information 
 to ensure the GNU AFFERO GENERAL PUBLIC LICENSE version 3.0 requirements will be met:
 http://www.gnu.org/licenses/agpl.html

 There is a plugin exception available for use with this release of translate5 for
 open source applications that are distributed under a license other than AGPL:
 Please see Open Source License Exception for Development of Plugins for translate5
 http://www.translate5.net/plugin-exception.txt or as plugin-exception.txt in the root
 folder of translate5.
  
 @copyright  Marc Mittag, MittagQI - Quality Informatics
 @author     MittagQI - Quality Informatics
 @license    GNU AFFERO GENERAL PUBLIC LICENSE version 3 with plugin-execptions
			 http://www.gnu.org/licenses/agpl.html http://www.translate5.net/plugin-exception.txt

END LICENSE AND COPYRIGHT
*/

class Editor_SegmentskeylogController extends editor_Controllers_EditorrestController {

    protected $entityClass = 'editor_Models_Segmentskeylog';

    /**
     * @var editor_Models_Segment
     */
    protected $entity;
    
    /**
     * @var ZfExtended_EventManager
     */
    protected $events = false;
    
    
    /**
     * Initialize event-trigger.
     * 
     * For more Information see definition of parent-class
     * 
     * @param Zend_Controller_Request_Abstract $request
     * @param Zend_Controller_Response_Abstract $response
     * @param array $invokeArgs
     */
    public function __construct(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array())
    {
        parent::__construct($request, $response);
        $this->events = ZfExtended_Factory::get('ZfExtended_EventManager', array(get_class($this)));
    }
    
    
    public function init() {
      parent::init();
    }

    public function indexAction() {
        $taskGuid = $this->session->taskGuid;
        $sessionUser = new Zend_Session_Namespace('user');
        $userGuid = $sessionUser->data->userGuid;
        $this->view->rows = $this->entity->loadByTaskAndUser($taskGuid, $userGuid);
    }



    public function putAction() {
        throw new ZfExtended_BadMethodCallException(__CLASS__ . '->put');
    }
    
    public function getAction() {
        $taskGuid = $this->session->taskGuid;
        $sessionUser = new Zend_Session_Namespace('user');
        $userGuid = $sessionUser->data->userGuid;
        $segmentId = $this->_getParam("segmentId");
        $this->view->rows = $this->entity->loadByFK($segmentId, $taskGuid, $userGuid);
    }

    public function deleteAction() {
        throw new ZfExtended_BadMethodCallException(__CLASS__ . '->put');
    }

    public function postAction() {
        $taskGuid = $this->session->taskGuid;
        $sessionUser = new Zend_Session_Namespace('user');
        $userGuid = $sessionUser->data->userGuid;

        $this->entity->setUserGuid($userGuid);
        $this->entity->setTaskGuid($taskGuid);


        $this->decodePutData();
        $this->entity->setSegmentId($this->data->segmentId);

        $this->entity->setLog($this->data->log);

        $this->entity->setStartTime($this->data->startTime);
        $this->entity->setEndTime($this->data->endTime);

        $this->entity->save();
        $this->view->row = $this->entity->getDataObject();
    }
}